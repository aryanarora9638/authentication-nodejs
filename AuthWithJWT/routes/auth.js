let router = require('express').Router()
const User = require('../models/User')
let bcrypt = require('bcryptjs')
let jwt = require('jsonwebtoken')
let verify = require('../validation/verifyJWT')

//Validation
//we use a package to validate all the user credentials
//npm install --save @hapi/joi
let {registerValidation, loginValidation}  = require('../validation/validation')


router.post('/register', async (req, res) => {

    //Validate the user
    let validation = registerValidation(req.body)
    if(validation.error){
        return res.status(400).send(validation.error.details[0].message)
    }

    //check if the user already exists
    let emailExist = await User.findOne({email : req.body.email})
    if(emailExist) {
        return res.status(400).send("Email Already exists")
    }

    //Hash the password
    //we use a package called bcryptjs
    //npm install --save bcryptjs
    let salt = await bcrypt.genSalt(10)
    //complexity of the pass is decided by the salt, with length 10 (genSalt)
    let hashPassword = await bcrypt.hash(req.body.password, salt)

    //Make the new user
    let user = new User({
        name : req.body.name,
        email: req.body.email,
        password : hashPassword
    })

    //Save user to the db
    try {
        let savedUser = await user.save()
        res.send(savedUser)
    }catch (e) {
        res.status(400).send(e)
    }
})

router.post('/login', async (req, res) => {

    //Validate the user
    let validation = loginValidation(req.body)
    if(validation.error){
        return res.status(400).send(validation.error.details[0].message)
    }

    //check if the user already exists
    let user = await User.findOne({email : req.body.email})
    if(!user) {
        return res.status(400).send("Email doesn't exists")
    }

    //If password is correct
    let validatePassword = await bcrypt.compare(req.body.password, user.password)
    //order of the parameters matter in the bcrypt compare
    console.log(validatePassword)
    if(!validatePassword) {
        return res.status(400).send("Invalid Password")
    }

    //Create and assign a token
    let token = jwt.sign({_id : user._id}, process.env.TOKEN_SECRET)
    res.header('auth_token', token).send(token)
})

router.get('/', (req, res) => {
    res.send("Home page")
})

router.get('/privateRoute', verify, (req,res) => {
    res.send("Access granted")
})

module.exports = router