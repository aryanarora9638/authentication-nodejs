let express = require('express')
let app = express()
let mongoose = require('mongoose')


//Import Routes
let authRoute = require('./routes/auth')

//Connect to DB
/*
   npm install --save dotenv
   So instead to hard coding the password in here, we will use a package called dotenv
   that stores the environment variables, that are not send to the user but injected
   into the application when requested. Keeping the security of the application intact
      */
let dotenv = require('dotenv')
dotenv.config()

mongoose.connect(
    process.env.DB_CONNECT,
    { useNewUrlParser : true},
    () => {
        console.log('Connected to DB')
    })

//Middleware
app.use(express.json())

//Route MiddleWare
app.use('/api/user', authRoute)

app.listen(3001, () => {
    console.log("Server Running!!")
})

