let mongoose = require('mongoose')

let userSchema = new mongoose.Schema({
    name : {
        type : String,
        required : true,
        min : 3
    },
    email : {
        type : String,
        required: true,
        max : 255,
        min : 3
    },
    password : {
        type : String,
        required : true,
        min : 7,
        max: 1024
    },
    date : {
        type : Date,
        default : Date.now()
    }
})

module.exports = mongoose.model('User', userSchema)