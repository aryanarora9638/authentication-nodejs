//Validation
//we use a package to validate all the user credentials
//npm install --save @hapi/joi

let Joi = require('@hapi/joi')
//defining a schema for our validation process

let registerValidation = (data) => {
    //defining a schema for our validation process
    let schema = {
        name: Joi.string()
            .min(3)
            .required(),

        email: Joi.string()
            .min(3).email()
            .required(),

        password: Joi.string()
            .min(3)
            .required()
    }
    return Joi.validate(data,schema)
}

let loginValidation = (data) => {
    //defining a schema for our validation process
    let schema = {
        email: Joi.string()
            .min(3).email()
            .required(),

        password: Joi.string()
            .min(3)
            .required()
    }
    return Joi.validate(data,schema)
}

module.exports.registerValidation = registerValidation
module.exports.loginValidation = loginValidation
