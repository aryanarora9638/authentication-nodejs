let jwt = require('jsonwebtoken')

verify = (req, res, next) => {
    let token = req.header('auth_token')
    if(!token){
        return res.status(401).send("Access denied")
    }

    try {
        let verified = jwt.verify(token, process.env.TOKEN_SECRET)
        req.user = verified
        next()
    }
    catch (e) {
        return res.status(401).send("Invalid Token")
    }
}

module.exports = verify