let mongoose = require('mongoose'),
    passportLocalMongoose = require('passport-local-mongoose')

let userSchema = mongoose.Schema({
    username : String,
    password : String
})

//adding passport local to user schema
userSchema.plugin(passportLocalMongoose)
//adds all the methods that comes with the passport local to the userSchema

module.exports = mongoose.model('User', userSchema)