//User authentication is can be created all manually or by using several tools in the npm
//Here we will use -
// Passport.js
//Passport Local
//Passport Local mongoose
//And Express sessions

//Basic work-flow
//Sessions -
// sessions are required to keep the user logged in even if we routes through
//different url's.
//Http is stateless and thus cannot contain any information, therefore we use sessions to
//store some information which when passed to the server, it can decrypt it and authenticate the user
//without having the user to do it manually again and thus keep him logged in even when the user
//routes to different url's

//npm install ejs express express-session body-parser mongoose --save
//npm install passport passport-local passport-local-mongoose  --save


//=========
//BOILERPLATE
//=========

let express               = require('express'),
    mongoose              = require('mongoose'),
    passport              = require('passport'),
    bodyParser            = require('body-parser'),
    LocalStrategy         = require('passport-local'),
    passportLocalMongoose = require('passport-local-mongoose'),
    User                  = require('./models/user');

mongoose.connect('mongodb://localhost/authentication_nodejs')

let app = express()

app.use(bodyParser.urlencoded({extended : true}))

app.use(passport.initialize())
app.use(passport.session())

app.use(require('express-session')({
    secret : "secret used to encrypt and decrypt the sessions",
    resave : false,
    saveUninitialized : false
}))

//Responsible of reading the session, taking data form the session encoding and decoding it respectively
passport.serializeUser(User.serializeUser())
passport.deserializeUser(User.deserializeUser())
passport.use(new LocalStrategy(User.authenticate()))

//=========
//ROUTES
//=========

app.get('/', (req, res) => {
    res.render('home.ejs')
})

isLoggedIn = (req, res, next) => {
    if(req.isAuthenticated()){
        return next()
    }
    res.redirect('/login')
}

//added a middleware (isLoggedIn) so the user cannot access the profile page without logging in
app.get('/profile', isLoggedIn ,(req, res) => {
    res.render('profile.ejs')
})



//=========
//AUTH ROUTES
//=========

//sign-up form
app.get('/register', (req, res) => {
    res.render('register.ejs')
})

//sign-up received
app.post('/register', (req, res) => {
    let newUser = new User({
        username : req.body.username
    })
    User.register(newUser, req.body.password, (error, user) => {
        if(error){
            console.log(error)
            return res.render('/register')
        }
        console.log(user)
        passport.authenticate('local')(req, res, () => {
            res.redirect('/profile')
        })
    })
})

app.get('/login', (req, res) => {
    res.render('login.ejs')
})

//we use middle-ware here
//middle-ware is the piece of code that gets executed before the main call back gets executed
app.post(
    '/login',
    passport.authenticate("local", {
        successRedirect : "/profile",
        failureRedirect : '/login'
    }),
    (req, res) => {
        res.send("Logged in")
    })

app.get('/logout', (req, res) => {
    req.logout()
    res.redirect('/')
})

app.listen(3001, () => {
    console.log("Server Running on port 3001")
})